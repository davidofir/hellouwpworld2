﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HelloUWP
{
    /// <summary>
    /// The UI for this single-page application. Allows the user to press a button 
    /// to see the image
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Declare a field variable to represent the image asset being displayed so that the variable
        /// can be used in multiple methods. Field variables are declared using their type and name in the
        /// class scope
        /// </summary>
        BitmapImage _avatarImage;

        /// <summary>
        /// The constructor of the class
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            //initialize the field variable _avatarImage. Note how the type is not being identified when the variable
            //is used. Specifying the type would declare a new variable with the same name, local to the constructor!
            _avatarImage = new BitmapImage(new Uri("ms-appx:///Assets/HomerSimpson2.gif"));
        }

        /// <summary>
        /// Event handler that is called when the user clicks on _btnSayHello
        /// </summary>
        /// <param name="sender">The button _btnSayHello</param>
        /// <param name="e">not used</param>
        private void OnSayHello(object sender, RoutedEventArgs e)
        {
            //set the source of the image to the image asset 
            _imgHello.Source = _avatarImage;

            //change the caption of the button
            _btnHello.Content = "Tap the image to help Homer";
        }

        /// <summary>
        /// BONUS: Event handler called when the user taps on the image. If the image is an animated
        /// GIF the tap wil play or stop the animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnImageTapped(object sender, TappedRoutedEventArgs e)
        {
            if (_avatarImage.IsAnimatedBitmap)
            {
                //toggle the animation 
                if (_avatarImage.IsPlaying)
                {
                    _avatarImage.Stop();
                }
                else
                {
                    _avatarImage.Play();
                }
            }

        }
    }
}
